﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Envio
{
    class CaravanSprite
    {
        public Texture2D texture;
        private RouteObject route;
        private Vector2 position;
        private List<string> goodsBay = new List<string>();

        public CaravanSprite(Texture2D Texture, RouteObject Route)
        {
            texture = Texture;
            route = Route;
        }

        public void Update()
        {

            // Advance to the next part of the route. See function for more info.
            this.FollowRoute(); 
        }

        /// <summary>
        /// Basic "Draw" method, draws the caravan sprite on screen
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch to draw onto</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
        /// <summary>
        /// Advances the position in the route being followed
        /// </summary>
        public void FollowRoute()
        {
            position = route.GetNextTilePos();
            // Add logic for stops
        }

    }
}
