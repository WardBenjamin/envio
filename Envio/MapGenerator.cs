﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Envio
{
    class MapGenerator
    {
        public List<Tile> tileList = new List<Tile>();
        private int tilesGenerated = 0;
        private int specialTilesGenerated = 0;
        int rows;
        int columns;

        ContentManager contentManager;
        private Dictionary<string, Texture2D> assets = new Dictionary<string, Texture2D>();
        private Dictionary<string, Vector2> assetSizes = new Dictionary<string,Vector2>();

        // Declare every single texture used in the generator/loader. Note: Temporary solution
        // I cannot believe this was actually temporary!
        /* string landAsset;
        Texture2D landTex;
        Vector2 landTexSize;
        string waterAsset;
        Texture2D waterTex;
        Vector2 waterTexSize;
        string town1Asset;
        Texture2D town1Tex;
        Vector2 town1TexSize;
        string town2Asset;
        Texture2D town2Tex;
        Vector2 town2TexSize;
        string town3Asset;
        Texture2D town3Tex;
        Vector2 town3TexSize; */

        public MapGenerator(ContentManager contentManager, int rowNum, int columnNum, List<string> assetList)
        {
            
            rows = rowNum;
            columns = columnNum;

            this.SetContentManager(contentManager);
            this.LoadAssets(assetList);
            this.GenerateMap();
        }
        public void LoadAssets(List<string> assetList)
        {
            var assetsToLoad = assetList;
            foreach (var asset in assetsToLoad)
            {
                assets.Add(asset, this.LoadContent(asset));
            }
        }

        public void SetContentManager(ContentManager cManager)
        {
            contentManager = cManager;
        }

        private Texture2D LoadContent(string assetName)
        {
            Texture2D asset = contentManager.Load<Texture2D>(assetName);
            assetSizes.Add(assetName, new Vector2(asset.Width, asset.Height));
            Console.WriteLine("Alert (MapGen): Asset " + assetName + " has been loaded.");

            return asset;
        }

        private void GenerateMap()
        {            
            // Load map template and create tiles
            string line;
            int x = 0;
            int y = 0;

            // Pick a map to load
            Random random = new Random();
            int mapNum = random.Next(1, 4);
            string map = "";
            switch (mapNum)
            {
                case 1:
                    map = "Content/!mapTownsTest.mapfmt";
                    Console.WriteLine("Alert (MapGen): Map 1 'Towns Test' has been selected.");
                    break;
                case 2:
                    map = "Content/!mapPangea.mapfmt";
                    Console.WriteLine("Alert (MapGen): Map 2 'Pangea' has been selected.");
                    break;
                case 3:
                    map = "Content/!mapBosphorus.mapfmt";
                    Console.WriteLine("Alert (MapGen): Map 3 'Bosphorus' has been selected.");
                    break;
                default:
                    map = "Content/!mapTownsTest.mapfmt";
                    Console.WriteLine("Alert (MapGen): Default Map (Map1 'Towns Test') has been selected."); // This should never happen. Hopefully.
                    Console.WriteLine("Warning (MapGen): This should not have happened. Check MapGenerator.cs to figure out what broke.");
                    break;

            }
            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader(@map);
            while ((line = file.ReadLine()) != null)
            {
                // Retrieve each character from each line
                foreach (var curChar in line)
                {
                    if (curChar == 'l')
                    {
                        Tile tile = new Tile(/* Pass in default z coordinate */ 0.1f);
                        tile.SetTexture(assets["landTex"], assetSizes["landTex"]);
                        tile.SetPosition(x, y);
                        string name = ("Tile" + tilesGenerated.ToString());
                        tile.SetName(name);
                        tileList.Add(tile);
                        tilesGenerated++;
                        // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                        x += 48;
                    }
                    else if (curChar == 'w') {
                        Tile tile = new Tile(/* Pass in default z coordinate */ 0.1f);
                        tile.SetTexture(assets["waterTex"], assetSizes["waterTex"]);
                        tile.SetPosition(x, y);
                        string name = ("Tile" + tilesGenerated.ToString());
                        tile.SetName(name);
                        tileList.Add(tile);
                        tilesGenerated++;
                        // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                        x += 48;
                    }
                    else if (curChar == 't')
                    {
                        // Create a tile, then create a town object on top of it
                        Tile tile = new Tile(/* Pass in default z coordinate */ 0.1f);
                        tile.SetTexture(assets["landTex"], assetSizes["landTex"]); // Why would a town be built on water, anyway?
                        tile.SetPosition(x, y);
                        string name = ("Tile" + tilesGenerated.ToString());
                        tile.SetName(name);
                        tileList.Add(tile);
                        tilesGenerated++;
                        // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                        Town town = new Town(/* Pass in default z coordinate */ 0.15f);
                        name = ("SpecialTile" + specialTilesGenerated.ToString() + " (Town)");
                        Random randomGen = new Random(((((((tilesGenerated*7)-3)/4)+13)*29)-5)/32); // Absolutely random math to screw up the number a bit
                        int townTexNum = randomGen.Next(1, 4);
                        switch (townTexNum)
                        {
                            case 1:
                                town.SetTexture(assets["town1Tex"], assetSizes["town1Tex"]);
                                Console.WriteLine("Alert (MapGen): Town Image 1 has been selected for " + name + ".");
                                break;

                            case 2:
                                town.SetTexture(assets["town2Tex"], assetSizes["town2Tex"]);
                                Console.WriteLine("Alert (MapGen): Town Image 2 has been selected for " + name + ".");
                                break;

                            case 3:
                                town.SetTexture(assets["town3Tex"], assetSizes["town3Tex"]);
                                Console.WriteLine("Alert (MapGen): Town Image 3 has been selected for " + name + ".");
                                break;

                            default:
                                town.SetTexture(assets["town1Tex"], assetSizes["town1Tex"]); // Again, this should never, ever happen. 
                                Console.WriteLine("Alert (MapGen): Default town image (town1Tex) has been selected for " + name + ".");
                                Console.WriteLine("Warning (MapGen): This should not have happened. Check MapGenerator.cs to figure out what broke.");
                                break;

                        }
                        town.SetPosition(x, y);
                        tileList.Add(town);
                        specialTilesGenerated++;
                        Console.WriteLine("Alert (MapGen): " + name + " has been created");

                        x += 48;
                    }
                }
                x = 0;
                y += 48;
                                    
            }

            file.Close();
            // TODO: Finish this statement when you are awake enough to remember how to.
            //Console.WriteLine("MapGenerator has created {} tiles and {} special tiles.");
            
        }
        private void GenerateMapDepricated()
        {
            // For each row and each column, generate a tile
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    // Create a tile, load it's texture, and set it's position
                    // Note: creates only land tiles
                    Tile tile = new Tile(/* Pass in the default z coordinate */ 0.1f);
                    tile.SetTexture(assets["landTex"], assetSizes["landTex"]);
                    tile.SetPosition(new Vector2(i, j));
                    tileList.Add(tile);
                    tilesGenerated++;

                }
            }
        }
    }
}
