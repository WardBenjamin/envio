﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Envio
{
    class RouteObject
    {
        private Vector3 color;
        private List<Vector2> tilePositions = new List<Vector2>();
        private int tileNum = 0; // Number to reference which tile the caravan/etc is on
        private int totalTileNum = 0;
        // Stores the direction for the caravan or ship assigned to the route
        private DirectionObject Direction = new DirectionObject();
        public bool direction = true;

        public RouteObject(Vector3 Color, Vector2 InitialPos)
        {
            tilePositions.Add(InitialPos); // Set the initial position to the first item in the list
            totalTileNum++;
            color = Color;
        }
        public Vector2 GetNextTilePos()
        {
            if (direction = Direction.forward)
            {
                tileNum++;
                if (tileNum > totalTileNum)
                {
                    tileNum--;
                    direction = Direction.backwards;
                    // logic to trigger a stop
                }
            }
            else if (direction == Direction.forward)
            {
                tileNum--;
                if (tileNum < 0)
                {
                    tileNum++;
                    direction = Direction.backwards;
                    // logic to trigger stop
                }
            }
            return tilePositions[tileNum];
        }
    }
}
