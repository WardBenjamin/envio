﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Envio
{
    class fpsCounter
    {
        private int total_frames = 0;
        private float elapsed_time = 0.0f;
        private int fps = 0;
        private SpriteFont spriteFont;

        public fpsCounter()
        {

        }
        public void Update(GameTime game_time)
        {
            elapsed_time += (float)game_time.ElapsedGameTime.TotalMilliseconds;

            // 1 Second has passed
            if (elapsed_time >= 1000.0f)
            {
                fps = total_frames;
                total_frames = 0;
                elapsed_time -= 1000.0f;
            }

        }
        public void SetFont(SpriteFont spr_font)
        {
            spriteFont = spr_font;
        }
        public void Draw(SpriteBatch sprite_batch)
        {
            total_frames++;

            sprite_batch.DrawString(spriteFont, string.Format("FPS: {0}", fps), new Vector2(10.0f, 20.0f), Color.White);
        }
    }
}
