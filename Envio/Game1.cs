﻿#region Using Statements
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace Envio
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<string> guiAssets = new List<string>();
        private GUI GUI;

        private MouseState oldMouseState;

        private SpriteFont _spr_font;
        private fpsCounter _fps_counter;
        private List<Sprite> drawablesList = new List<Sprite>();
        private List<Sprite> objectList = new List<Sprite>();
        private List<Tile> tileList;

        public int screenWidth = 1024;
        public int screenHeight = 768;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferHeight = screenHeight;
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.ApplyChanges();

            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: Load textures here
            _spr_font = Content.Load<SpriteFont>("Pescadero");

            // TODO: use this.Content to load your game content here
            _fps_counter = new fpsCounter();
            _fps_counter.SetFont(_spr_font);

            this.GUI = new GUI(this);

            // Generate map and add tiles to the list of drawable items
            List<string> mapAssets = new List<string>();
            // Read the asset names needed from a file
            System.IO.StreamReader file = new System.IO.StreamReader(@"Content/mapAssets.assetFile");
            string line;
            while ((line = file.ReadLine()) != null)
            {
                mapAssets.Add(line);
            }
            MapGenerator mapGen = new MapGenerator(this.Content, 21, 16, mapAssets);
            this.tileList = mapGen.tileList;
            foreach (var tile in mapGen.tileList)
            {
                this.drawablesList.Add(tile);
                this.objectList.Add(tile);
            }
            this.GUI.SetContentManager(this.Content);
            guiAssets.Add("guiSquare");
            this.GUI.LoadAssets(guiAssets);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Check for clicks
            MouseState newMouseState = Mouse.GetState();

            if (newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
            {
                // Click has just occured
                Point mousePos = new Point(newMouseState.X, newMouseState.Y);
                
                // Check if the click is on the GUI
                var  clickedElement = this.GUI.elements.Where(e => e.GetRect().Contains(mousePos)).OrderBy(i => i.GetZ()).FirstOrDefault();
                if (clickedElement != null)
                {
                    clickedElement.ClickedAction();
                    Console.Out.WriteLine("Alert (Game1): Clicked GUI Element. Object Z value: {0}.", Convert.ToString(clickedElement.GetZ()));
                }
                else // Click must be on a tile or other object
                {
                    var clickedObject = this.objectList.Where(e => e.GetRect().Contains(mousePos)).OrderBy(i => i.GetZ()).FirstOrDefault();
                    if (clickedObject != null)
                    {
                        clickedObject.ClickedAction();
                        Console.Out.WriteLine("Alert (Game1): Clicked Object. Object Z value: {0}.", Convert.ToString(clickedObject.GetZ()));
                    }
                    else
                    {
                        // Throw Exception
                    }
                }
            }

            oldMouseState = newMouseState;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Iterate through tile list and update each tile
            foreach (var tile in tileList)
            {
                tile.Update();
            }

            // Update FPS
            _fps_counter.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // TODO: Add your drawing code here
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            
            foreach (var sprite in drawablesList)
            {
                sprite.Draw(spriteBatch);
            }

            this.GUI.Draw(spriteBatch);

            // FPS must be drawn on top
            _fps_counter.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
