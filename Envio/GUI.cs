﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Envio
{
    class GUI
    {
        public List<guiElement> elements = new List<guiElement>();
        int elementsGenerated = 0;
        public string selectedItem = "";
        Dictionary<string, Texture2D> assets = new Dictionary<string, Texture2D>();
        Dictionary<string, Vector2> assetSizes = new Dictionary<string, Vector2>();
        ContentManager contentManager;
        Game1 parentGame;

        public GUI(Game1 pGame) 
        {
            parentGame = pGame;
        }

        public void LoadAssets(List<string> assetList) 
        {
            var assetsToLoad = assetList;
            foreach (var asset in assetsToLoad)
            {
                assets.Add(asset, this.LoadContent(asset));       
            }
            this.ConstructGUIElements();
        }

        public void SetContentManager(ContentManager cManager)
        {
            contentManager = cManager;
        }

        private Texture2D LoadContent(string assetName)
        {
            Texture2D asset = contentManager.Load<Texture2D>(assetName);
            assetSizes.Add(assetName, new Vector2(asset.Width, asset.Height));
            Console.WriteLine("Alert (GUI): Asset " + assetName + " has been loaded.");

            return asset;
        }
        private void ConstructGUIElements()
        {
            ConstructGUINormalMode();
            ConstructGUIRouteMode();
        }

        private void ConstructGUINormalMode()
        {
            string line;
            int topElements = 0;
            int bottomElements = 0;
            int leftElements = 0;
            int rightElements = 0;

            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader(@"Content/layout1.guiLayout");
            while ((line = file.ReadLine()) != null)
            {
                // Count the number of each element to generate
                foreach (var curChar in line)
                {
                    if (curChar == 't')
                    {
                        topElements++;
                    }
                    else if (curChar == 'b') 
                    {
                        bottomElements++;
                    }
                    else if (curChar == 'l') 
                    {
                        leftElements++;
                    }
                    else if (curChar == 'r') 
                    {
                        rightElements++;
                    }
                }
            }
            
            file.Close();

            int x = 0;
            int y = 0;
            int yForSides = 0; // Used so that the sides never overlap with the top of the GUI.
            int topElementsGenerated = 0;
            for (int i = 0; i < topElements; i++)
			{
                // Create an element and pass in side
			    guiElement element = new guiElement("top", /* Pass in default z coordinate */ 0.5f);
                element.SetTexture(assets["guiSquare"], assetSizes["guiSquare"]);
                element.SetPosition(x, 0);
                string name = ("GuiElement" + elementsGenerated.ToString());
                element.SetName(name);
                elements.Add(element);
                // To prevent overlap between the top row and the side rows
                if (topElementsGenerated == 0)
                {
                    yForSides = element.GetRect().Height;
                    topElementsGenerated++;
                }
                else
                {
                    topElementsGenerated++;
                }
                elementsGenerated++;
                // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                x += element.GetRect().Width;
			}

            x = 0;
            for (int j = 0; j < bottomElements; j++)
            {
                // Create an element and pass in side
                guiElement element = new guiElement("bottom", /* Pass in default z coordinate */ 0.5f);
                element.SetTexture(assets["guiSquare"], assetSizes["guiSquare"]);
                element.SetPosition(x, parentGame.screenHeight - element.GetRect().Height);
                string name = ("GuiElement" + elementsGenerated.ToString());
                element.SetName(name);
                elements.Add(element);
                elementsGenerated++;
                // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                x += element.GetRect().Width;
            }

            x = 0;
            y = yForSides;
            for (int k = 0; k < leftElements; k++)
            {
                // Create an element and pass in side
                guiElement element = new guiElement("left", /* Pass in default z coordinate */ 0.5f);
                element.SetTexture(assets["guiSquare"], assetSizes["guiSquare"]);
                element.SetPosition(x, y);
                string name = ("GuiElement" + elementsGenerated.ToString());
                element.SetName(name);
                elements.Add(element);
                elementsGenerated++;
                // Console.WriteLine("Alert: " + name + " has been created"); // For Debugging

                y += element.GetRect().Height;
            }

            y = yForSides;
            for (int l = 0; l < leftElements; l++)
            {
                // Create an element and pass in side
                guiElement element = new guiElement("right", /* Pass in default z coordinate */ 0.5f);
                element.SetTexture(assets["guiSquare"], assetSizes["guiSquare"]);
                element.SetPosition(parentGame.screenWidth - element.GetRect().Width, y);
                string name = ("GuiElement" + elementsGenerated.ToString());
                element.SetName(name);
                elements.Add(element);
                elementsGenerated++;
                Console.WriteLine("Alert: {0} has been created at {1}", name, new Vector2(element.GetRect().X, element.GetRect().Y).ToString()); // For Debugging

                y += element.GetRect().Height;
            }
        }

        private void ConstructGUIRouteMode()
        {

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var element in elements)
            {
                element.Draw(spriteBatch);
            }
        }
    }
}