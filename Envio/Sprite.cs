﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Envio
{
    class Sprite
    {
        string name;
        Vector2 position;
        Rectangle rect;
        Texture2D texture;
        Vector2 textureSize;
        private float z_coordinate = 0.0f;

        public Sprite() { }

        public Sprite(float z_coord) 
        {
            z_coordinate = z_coord;
        }
        /// <summary>
        /// Depricated
        /// </summary>
        public Sprite(Vector2 tilepos, Texture2D tex)
        {
            float texWidth = tex.Width;
            float texHeight = tex.Height;
            position = new Vector2(tilepos.X * texWidth, tilepos.Y * texHeight);
            rect = new Rectangle((int)position.X, (int)position.Y, (int)texWidth, (int)texHeight);
            texture = tex;
            textureSize = new Vector2(texWidth, texHeight);
        }

        /// <summary>
        /// Loads the texture for the sprite object.
        /// </summary>
        /// <param name="contentManager">Content manager from the main Game class</param>
        /// <param name="assetName">Name of asset to load</param>
        public void SetTexture(Texture2D tex, Vector2 texSize)
        {
            texture = tex;
            textureSize = texSize;
            this.UpdateRect();
        }
        /// <summary>
        /// Sets the position of the sprite object. Accepts positions from a grid, and multiplies by the size of it's texture.
        /// </summary>
        /// <param name="gridPos"></param>
        public void SetPosition(Vector2 gridPos)
        {
            // Transform the grid coordinates into screen coordinates
            this.position = new Vector2(gridPos.X * textureSize.X, gridPos.Y * textureSize.Y);
            this.UpdateRect();
        }
        /// <summary>
        /// Sets the position of the sprite object. Accepts screen position x and screen position y
        /// </summary>
        /// <param name="pos"></param>
        public void SetPosition(int x, int y)
        {
            this.position = new Vector2(x, y);
            this.UpdateRect();
        }

        private void UpdateRect()
        {
            rect = new Rectangle((int)position.X, (int)position.Y, (int)textureSize.X, (int)textureSize.Y);
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(texture, position);
        }
        public void Draw()
        {
            // Overload
        }
        public void Update()
        {

        }
        public Rectangle GetRect()
        {
            return this.rect;
        }
        public float GetZ()
        {
            return this.z_coordinate;
        }
        public void ClickedAction()
        {

        }
        public void SetName(string name)
        {
            this.name = name;
        }
    }
}
